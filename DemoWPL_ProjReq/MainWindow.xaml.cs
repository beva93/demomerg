﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoWPL_ProjReq
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string ruimte1lijst = "", ruimte2lijst = "", ruimte3lijst = "";
        public int lijst1Counter = 0, lijst2Counter = 0, lijst3Counter = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnRegistreer_Click(object sender, RoutedEventArgs e)
        {
            string naam = txtNaamInput.Text;
            int.TryParse(txtRuimteInput.Text, out int ruimte);
            if (isValidRuimte(ruimte))
            {
                MessageBox.Show($"Welkom {naam} in ruimte {ruimte}");
                if (isLijstNietVol(ruimte))
                {
                    switch (ruimte)
                    {
                        case 1:
                            ruimte1lijst += $"{naam}\r\n";
                            txtRuimte1.Text = ruimte1lijst;
                            break;
                        case 2:
                            ruimte2lijst += $"{naam}\r\n";
                            txtRuimte2.Text = ruimte2lijst;
                            break;
                        case 3:
                            ruimte3lijst += $"{naam}\r\n";
                            txtRuimte3.Text = ruimte3lijst;
                            break;
                        default:
                            break;
                    }
                    
                }
                else
                {
                    MessageBox.Show($"Het maximaale toegestane aanwezigen voor ruimte {ruimte} is bereikt.");

                }
                txtRuimteInput.Clear();
                txtNaamInput.Clear();
            }
        }

        private void btnAfsluiten_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Bent u zeker dat u de applicatie wil afsluiten?", "Afsluiten", MessageBoxButton.YesNo);
            if(messageBoxResult == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        private bool isValidRuimte(int ruimte)
        {
            bool isValid = (ruimte >= 1 && ruimte <= 3);
            return isValid;
        }

        private bool isLijstNietVol(int ruimte)
        {
            switch (ruimte)
            {
                case 1:
                    ruimte1lijst += $"{++lijst1Counter}. ";
                    return lijst1Counter <= 10;
                case 2:
                    ruimte2lijst += $"{++lijst2Counter}. ";
                    return lijst2Counter <= 10;
                case 3:
                    ruimte3lijst += $"{++lijst3Counter}. ";
                    return lijst3Counter <= 10;
                default:
                    return false;
            }
        }

    }
}
